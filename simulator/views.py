# views.py
from coreapp import app
from flask import render_template, url_for 
import requests

@app.route('/')
def home():
	#Home view
	return render_template("home.html", message = '')
		
@app.route('/makepost/<username>/<room>/<device>/<command>',defaults={'msg': ''}, methods=['GET'])
@app.route('/makepost/<username>/<room>/<device>/<command>/<msg>', methods=['GET'])
def makepost(username,room,device,command,msg):
	username = str(username)
	room = str(room)
	device = str(device)
	command = str(command)
	msg = str(msg)
	url = 'http://shwa-carlos.herokuapp.com/inmsg/'+username+'/'+room+'/'+device+'/'+command+'/'+msg
	r = requests.get(url) # r = requests.get('https://api.github.com/user', auth=('user', 'pass'))
	return render_template("home.html", message = url+' | Status: '+str(r.status_code)+'. | Text: '+r.text)