#!shwa_env/bin/python
from app import db, models
from datetime import datetime

def add_admin(email_address, password):
    user = models.User.query.filter_by(email=email_address, 
           password=password).first()
    if user is None:
        u = models.User(email=email_address, nickname='uNickname', 
            password=password, first_name='uFirstName', last_name='uLastName', 
            date_registered=datetime.utcnow(), role = models.ROLE_USER)
        db.session.add(u)
        print "fake user account successfully added."
        #Add devices
        print "Populating devices..."
        for room in ["bedroom","bathroom","kitchen","living room"]:
            r = models.Room(name=room, user=u)
            db.session.add(r)
            print "Adding room " + r.name
            for device in ["lights","radio"]:
                d = models.Device(user=u, room= r, name=device, 
                url='http://smarthouse.com', 
                command='on', command_optional='off', request_type='GET', 
                last_used=datetime.utcnow(), message = 'Created')
                db.session.add(d)
                print 'Adding device '+d.name+' to room '+d.room.name
            for device in ["door","window"]:
                d = models.Device(user=u, room= r, name=device, 
                url='http://smarthouse.com/', 
                command='open', command_optional='close', request_type='GET', 
                last_used=datetime.utcnow(), message = 'Created')
                db.session.add(d)
                print 'Adding device '+d.name+' to room '+d.room.name
        db.session.commit()
        print "...done."
    else:
        print "fake user account already exists."

if __name__ == '__main__':
    #email_address = sys.argv[1]
    #password = sys.argv[2]
    add_admin('user@smarthouse.com', 'user')

