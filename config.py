import os
basedir = os.path.abspath(os.path.dirname(__file__))

SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(basedir, 'app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(basedir, 'db_repository')

LOG_FILE_PATH = os.path.join(basedir,'log.log')
MAX_LOG_ENTRY = 50

CSRF_ENABLED = True
SECRET_KEY = 'cryptoTokenToValidateForm'

OPENID_PROVIDERS = [
    { 'name': 'Google', 'url': 'https://www.google.com/accounts/o8/id' },
    { 'name': 'Yahoo', 'url': 'https://me.yahoo.com' },
    { 'name': 'AOL', 'url': 'http://openid.aol.com/<username>' },
    { 'name': 'Flickr', 'url': 'http://www.flickr.com/<username>' },
    { 'name': 'MyOpenID', 'url': 'https://www.myopenid.com' }]

HTTP_REQUESTS = [('GET','GET'),('POST','POST')]# Only GET and POST requests at the moment,('PUT','PUT'),('DELETE','DELETE'),
	#('OPTIONS','OPTIONS'),('HEAD','HEAD'),('TRACE','TRACE'),('CONNECT','CONNECT'),('PATCH','PATCH')]







