from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class EnableDisableAccount(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_enable_disable_account(self):
        driver = self.driver
        driver.get(self.base_url + "/register_mike")
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("m2")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("s2")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("m2")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Enable/Disable Accounts").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Disable Account')])[6]").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Sorry, you cannot log in because your account was disabled by an admin![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Enable/Disable Accounts").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Enable Account')])[1]").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*Welcome, m2![\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Delete Account").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
