from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class DMTest(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_d_m(self):
        driver = self.driver
	driver.get(self.base_url + "/register_mike")
        driver.get(self.base_url + "/home")
	driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("m2")
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("t")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("est")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Forum')])[3]").click()
        driver.find_element_by_link_text("Create new post").click()
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys("DM TEST")
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("dm")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Forum')])[3]").click()
        driver.find_element_by_link_text("m2").click()
        driver.find_element_by_link_text("Send Message").click()
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys("TEST")
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("can you read this????")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Messages").click()
        driver.find_element_by_link_text("TEST").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*can you read this[\s\S][\s\S][\s\S][\s\S][\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Reply").click()
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("yes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_css_selector("a.btn.btn-small").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Messages").click()
        driver.find_element_by_link_text("TEST").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*yes[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Sign out").click()
	driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m2@gmail.com")
	driver.find_element_by_id("password").clear()
	driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
 	driver.find_element_by_link_text("Manage Account").click()
        driver.find_element_by_link_text("Delete Account").click()
	
	driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("simulator@smarthouse.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("simulator")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("DM TEST").click()
        driver.find_element_by_link_text("Delete Post").click()
        driver.find_element_by_link_text("Sign out").click()
    
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
