from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class RegisterAndShare1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_register_and_share1(self):
        driver = self.driver
        driver.get(self.base_url + "/register")
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("u")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("l")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u1@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("u1")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("u")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("l")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u2@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("u2")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("u")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("l")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u3")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("u3")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u3@gmail.com")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u1@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Rooms").click()
        driver.find_element_by_link_text("Add room").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_room1")
        driver.find_element_by_id("webcam_url").clear()
        driver.find_element_by_id("webcam_url").send_keys("http://test.com")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Add room").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_room2")
        driver.find_element_by_id("webcam_url").clear()
        driver.find_element_by_id("webcam_url").send_keys("http://test.com")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Add device").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_device1")
        driver.find_element_by_id("url").clear()
        driver.find_element_by_id("url").send_keys("http://test.com/test1")
        driver.find_element_by_id("command").clear()
        driver.find_element_by_id("command").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Add device").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_device2")
        driver.find_element_by_id("url").clear()
        driver.find_element_by_id("url").send_keys("http://test.com/test2")
        driver.find_element_by_id("command").clear()
        driver.find_element_by_id("command").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Add device')])[2]").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_device3")
        driver.find_element_by_id("url").clear()
        driver.find_element_by_id("url").send_keys("http://test.com/test3")
        driver.find_element_by_id("command").clear()
        driver.find_element_by_id("command").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_xpath("(//a[contains(text(),'Add device')])[2]").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("test_device4")
        driver.find_element_by_id("url").clear()
        driver.find_element_by_id("url").send_keys("http://test.com/test4")
        driver.find_element_by_id("command").clear()
        driver.find_element_by_id("command").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Add users").click()
        driver.find_element_by_id("users-0-email").clear()
        driver.find_element_by_id("users-0-email").send_keys("u2")
        driver.find_element_by_id("add_user").click()
        driver.find_element_by_id("users-0-email").clear()
        driver.find_element_by_id("users-0-email").send_keys("u2@gmail.com")
        # ERROR: Caught exception [ReferenceError: selectLocator is not defined]
        driver.find_element_by_id("users-1-email").clear()
        driver.find_element_by_id("users-1-email").send_keys("u3@gmail.com")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_css_selector("a[alt=\"share test_device3\"]").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u2@gmail.com")
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        driver.find_element_by_css_selector("a[alt=\"share test_device3\"]").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u3@gmail.com")
        driver.find_element_by_css_selector("input[type=\"submit\"]").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_css_selector("a[title=\"Devices shared with me settings\"]").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_device3[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("a[title=\"Rooms shared with me settings\"]").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_room1 \(owner u1 \)[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("u3@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_css_selector("a[title=\"Devices shared with me settings\"]").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_device3[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_css_selector("a[title=\"Rooms shared with me settings\"]").click()
        # Warning: verifyTextPresent may require manual changes
        try: self.assertRegexpMatches(driver.find_element_by_css_selector("BODY").text, r"^[\s\S]*test_room1 \(owner u1 \)[\s\S]*$")
        except AssertionError as e: self.verificationErrors.append(str(e))
        driver.find_element_by_link_text("Sign out").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
