from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
import unittest, time, re

class DMPOT1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "http://shwa.herokuapp.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_d_m_p_o_t1(self):
        driver = self.driver
        driver.get(self.base_url + "/register")
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("d")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("m")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("dmPOT@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("dm")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Register").click()
        driver.find_element_by_id("first_name").clear()
        driver.find_element_by_id("first_name").send_keys("d")
        driver.find_element_by_id("last_name").clear()
        driver.find_element_by_id("last_name").send_keys("m2")
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("dmPOT2@gmail.com")
        driver.find_element_by_id("nickname").clear()
        driver.find_element_by_id("nickname").send_keys("dm2")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm_password").clear()
        driver.find_element_by_id("confirm_password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("dmPOT@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("Create new post").click()
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys("dm POT")
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("dm persists over time test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("dmPOT2@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("dm").click()
        driver.find_element_by_link_text("Send Message").click()
        driver.find_element_by_id("title").clear()
        driver.find_element_by_id("title").send_keys("dm POT")
        driver.find_element_by_id("body").clear()
        driver.find_element_by_id("body").send_keys("Can you see this 12 hours later?")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Sign out").click()
        driver.get(self.base_url + "/register_mike")
        driver.find_element_by_link_text("Sign in").click()
        driver.find_element_by_id("email").clear()
        driver.find_element_by_id("email").send_keys("m@gmail.com")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("tes")
        driver.find_element_by_css_selector("input.btn").click()
        driver.find_element_by_link_text("Forum").click()
        driver.find_element_by_link_text("dm POT").click()
        driver.find_element_by_link_text("Delete Post").click()
        driver.find_element_by_link_text("Sign out").click()
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException, e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert.text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
